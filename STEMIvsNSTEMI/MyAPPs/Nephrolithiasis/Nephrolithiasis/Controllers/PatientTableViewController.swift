//
//  PatientTableViewController.swift
//  Nephrolithiasis
//
//  Created by CMG on 11/22/20.
//

import Foundation
import UIKit
import CoreData
//import SwipeCellKit

class PatientTableViewController: UITableViewController {
    
    var patientArray = [Patient]()
    var patient: Patient?
    
    
   var stoneValues = StoneValues()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        loadPatient()
        print(stoneValues.labName)
        tableView.rowHeight = 80
    }
   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patientArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "patientCell", for: indexPath) as! SwipeTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "patientCell", for: indexPath)
        //cell.delegate = self
        cell.textLabel?.text = patientArray[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        patient = patientArray[indexPath.row]
        performSegue(withIdentifier: "goToLabData", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            
            self.context.delete(self.patientArray[indexPath.row])
            self.patientArray.remove(at: indexPath.row)
            self.savePatient()
            
            //print("index path of delete: \(indexPath)")
            completionHandler(true)
        }

        let rename = UIContextualAction(style: .normal, title: "EnterLab") { (action, sourceView, completionHandler) in
            
            self.patient = self.patientArray[indexPath.row]
            self.performSegue(withIdentifier: "goToEnterLab", sender: self)
        
            completionHandler(true)
        }
        
            
       
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [rename, delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "goToLabData" {
            let destinationVC = segue.destination as! LabTableViewController
            destinationVC.selectedPatient = patient
        }
        
        if segue.identifier == "goToEnterLab" {
            let destinationVC = segue.destination as! LabViewController
            destinationVC.selectedPatient = patient
            
        }

}
    
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let makeRead = UIContextualAction(style: .normal, title: "Results") { (action, sourceView, completionHandler) in
            
            let resultsVC = ResultsViewController()
            //print("Result", self.stoneValues.it!)
            resultsVC.it = self.stoneValues.abnormalReport
            resultsVC.two = self.stoneValues.normalReport
            resultsVC.three = "Comments"
            
//            
            self.present(resultsVC, animated: true, completion: nil)
               completionHandler(true)
           }
           let swipeActionConfig = UISwipeActionsConfiguration(actions: [makeRead])
           swipeActionConfig.performsFirstActionWithFullSwipe = false
           return swipeActionConfig
       }
    
    @IBAction func barButtonPressed(_ sender: UIBarButtonItem) {
        
        var textField = UITextField()
        
        let alert = UIAlertController(title: "New Patient", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Patient's Name", style: .default) { (action) in
                
            let newPatient = Patient(context:self.context)
            newPatient.name = textField.text!
            self.patientArray.append(newPatient)
            self.savePatient()
            
        }
        alert.addAction(action)
        alert.addTextField { (field) in
            textField = field
            textField.placeholder = "Add Patient"
            
        }
        present(alert, animated: true, completion: nil)
       
    }
    
    @IBAction func leftBarButtonPushed(_ sender: UIBarButtonItem) {

        
        let resultsVC = ResultsViewController()
        resultsVC.it = stoneValues.labName
        resultsVC.two = "Cat"
        resultsVC.three = "Rat"
        
        present(resultsVC, animated: true, completion: nil)
    }
    
    
    func savePatient() {
        
        do {
            try context.save()
        } catch {
            print("Error in saving context \(error)")
        }
        tableView.reloadData()
    }
    
    func loadPatient(request: NSFetchRequest<Patient> = Patient.fetchRequest()) {
        
        
        do {
            patientArray = try context.fetch(request)
        } catch {
            print("Error in fetching context \(error)")
        }
        tableView.reloadData()
    }
        
}
