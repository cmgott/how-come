//
//  LabViewController.swift
//  Nephrolithiasis
//
//  Created by CMG on 11/23/20.
//

import UIKit
import CoreData

class LabViewController: UIViewController {
    
    var labArray = [Lab]()
    
    var nextLab = Lab()
    var stoneValues = StoneValues()

    
    var lastValue:Float = 0.0
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
   
    @IBOutlet weak var labValue: UILabel!
    
    @IBOutlet weak var minSliderValue: UILabel!
    @IBOutlet weak var maxSliderValue: UILabel!
    
    @IBOutlet weak var urineCaLabel: UILabel!
    
    @IBOutlet weak var patientLab: UILabel!
    @IBOutlet weak var patientLab1: UILabel!
    @IBOutlet weak var patientLab2: UILabel!
    @IBOutlet weak var patientLab3: UILabel!
    @IBOutlet weak var patientLab4: UILabel!
    @IBOutlet weak var patientLab5: UILabel!
    @IBOutlet weak var patientLab6: UILabel!
    @IBOutlet weak var patientLab7: UILabel!
    @IBOutlet weak var patientLab8: UILabel!
    @IBOutlet weak var patientLab9: UILabel!
    @IBOutlet weak var patientLab10: UILabel!
    @IBOutlet weak var patientLab11: UILabel!
    @IBOutlet weak var patientLab12: UILabel!
    @IBOutlet weak var patientLab13: UILabel!
    @IBOutlet weak var patientLab14: UILabel!
    @IBOutlet weak var patientLab15: UILabel!
    
    
    @IBOutlet weak var sliderValue: UISlider!
    
    var selectedPatient : Patient? {
        didSet {
            //print("Did happen")
            //loadLab()
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        minSliderValue.text = "0"
        maxSliderValue.text = "11"
        
        
        
        //loadLab()
        
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        //let nextLab = Lab(context: context)
        nextLab = Lab(context: context)
        
        let labLabelArray = [patientLab, patientLab1, patientLab2, patientLab3, patientLab4, patientLab5, patientLab6, patientLab7, patientLab11, patientLab12, patientLab13, patientLab14, patientLab15]
        
        for n in 0...12 {
        print(n)
        if sender.tag == n {
            let labLabel = labLabelArray[n]
            labLabel!.text = labValue.text
            //patientLab.text = labValue.text
    
        }
    }
        if sender.tag == 13 {
            
            var textField = UITextField()
            
            let alert = UIAlertController(title: "Enter ssValue", message: "", preferredStyle: .alert)
            let action = UIAlertAction(title: "Add to 2 decimal places", style: .default) { (action) in
                
                self.patientLab8.text = textField.text
                self.lastValue = Float(self.patientLab8.text!) ?? 0.0
                //print(self.lastValue)
                //print(self.selectedPatient!)
                    
            }

            alert.addAction(action)
            alert.addTextField { (field) in
                textField = field
                textField.placeholder = "Add Value"
            }
            
            self.present(alert, animated: true, completion: nil)
            
            }
        
        if sender.tag == 14 {

         var textField = UITextField()

         let alert = UIAlertController(title: "Enter ssValue", message: "", preferredStyle: .alert)
         let action = UIAlertAction(title: "Add to 2 decimal places", style: .default) { (action) in

             self.patientLab9.text = textField.text
             self.lastValue = Float(self.patientLab9.text!) ?? 0.0
             print(self.lastValue)

         }
         alert.addAction(action)
         alert.addTextField { (field) in
             textField = field
             textField.placeholder = "Add Value"

         }
             present(alert, animated: true, completion: nil)

         }
        

       if sender.tag == 15 {

        var textField = UITextField()

        let alert = UIAlertController(title: "Enter ssValue", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Add to 2 decimal places", style: .default) { (action) in

            self.patientLab10.text = textField.text
            self.lastValue = Float(self.patientLab10.text!) ?? 0.0
            print(self.lastValue)

        }
        alert.addAction(action)
        alert.addTextField { (field) in
            textField = field
            textField.placeholder = "Add Value"

        }
            present(alert, animated: true, completion: nil)

        }
        
        nextLab.name = sender.currentTitle
        nextLab.value = lastValue
        selectedPatient?.addToLab(nextLab)
        labArray.append(nextLab)
        //saveLab()
        
        stoneValues.calculateLabValue(nextLab: nextLab.name!, lastValue: nextLab.value)
        
        
        
        print("From LabViewController" + stoneValues.abnormalReport)
    }

    
    @IBAction func slider(_ sender: UISlider) {
        lastValue = sender.value
        let value = String(format: "%.1f", sender.value)
        labValue.text = value
    }
    
    @IBAction func barButtonPressed(_ sender: UIBarButtonItem) {
        
        
        
        
        performSegue(withIdentifier: "goToResults", sender: ResultsViewController.self)
        
//        resultsVC.it = stoneValues.abnormalReport
//                    resultsVC.two = stoneValues.normalReport
//                    resultsVC.three = "REST"
        func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            if segue.identifier == "goToResults" {
                let destinationVC = segue.destination as! ResultsViewController
            }
        let resultsVC = ResultsViewController()

        resultsVC.it = "1"
        resultsVC.two = "2"
        resultsVC.three = "REST"
        
        }
//        self.present(resultsVC, animated: true, completion: nil)
      
        //}
 
    
//        func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "goToResults" {
//
//            let resultsVC = segue.destination as! ResultsViewController
//
////            resultsVC.it = stoneValues.abnormalReport
////            resultsVC.two = stoneValues.normalReport
////            resultsVC.three = "REST"
//
//        }
//    }
    
    }
    
    func saveLab() {
        do {

            try context.save()
        } catch {
            print("Error in daving context \(error)")
        }
    }

    
    func loadLab(){

        let request:NSFetchRequest<Lab> = Lab.fetchRequest()

        do {
            labArray = try context.fetch(request)
        } catch {
            print("Error in fetching context \(error)")
        }

    }

    
}
