//
//  LabTableViewController.swift
//  Nephrolithiasis
//
//  Created by CMG on 11/22/20.
//

import UIKit
import CoreData
//import SwipeCellKit

class LabTableViewController: UITableViewController {
    
    let context = (UIApplication.shared
                    .delegate as! AppDelegate).persistentContainer.viewContext
    
    var labArray = [Lab]()
    
    //property in receiving veiwController to get from sender
    //var stringFromFirst: String?
    
    var selectedPatient: Patient? {
        didSet {
            
//print("Did Work")
//print(selectedPatient?.name)
            
            loadLabReport()
            
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        tableView.rowHeight = 80
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labArray.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "labCell", for: indexPath) 
        //cell.textLabel?.textColor = .systemRed
        //cell.delegate = self
        
        
        //changed to selectedPatient from labArray[indexpath.row].name
        cell.textLabel?.text = labArray[indexPath.row].name! + ":   " + String(format: "%.1f", labArray[indexPath.row].value)
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 0
        
        return cell
        
        }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            
            self.context.delete(self.labArray[indexPath.row])
            self.labArray.remove(at: indexPath.row)
            self.saveLabReport()
            
            completionHandler(true)
        }
        
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
    
    
    func saveLabReport() {
        
        do {
            try context.save()
        } catch {
            print("Error in saving context \(error)")
        }
        tableView.reloadData()
    }
    
    func loadLabReport(){
        labArray = selectedPatient?.lab?.allObjects as! [Lab]
        
        
    }
}

//Mark: - WHY LAB VS LABS???????????????????????????????????????




//    func loadLabReport(with request:NSFetchRequest<Lab> = Lab.fetchRequest(), predicate: NSPredicate? = nil){
//
//
//
//            let request : NSFetchRequest<Lab> = Lab.fetchRequest()
//            //need to filter and then sort to get only what we want no all Items
//            let patientPredicate = NSPredicate(format: "Patient.name MATCHES %@", selectedPatient!.name!)
//
//        if let additionalPredicate = predicate {
//            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [patientPredicate, additionalPredicate])
//        } else {
//            request.predicate = patientPredicate
//        }
//           request.predicate = predicate
            
//            if let additionalPredicate = predicate {
//                request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [patientPredicate, additionalPredicate])
//            } else {
//                request.predicate = patientPredicate
//            }
        
        //let request:NSFetchRequest<Lab> = Lab.fetchRequest()
        
//        do {
//            labArray = try context.fetch(request)
//        } catch {
//            print("Error in fetching context \(error)")
//        }
        //tableView.reloadData()
//}



