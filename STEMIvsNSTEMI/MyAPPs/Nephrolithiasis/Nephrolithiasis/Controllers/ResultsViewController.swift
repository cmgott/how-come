//
//  ResultsViewController.swift
//  Nephrolithiasis
//
//  Created by CMG on 1/19/21.
//

import UIKit

class ResultsViewController: UIViewController {
    
    var it = "yes"
    var two = "2"
    var three = "3"
   
    
    @IBOutlet weak var topTextViewLabel: UITextView!
    
    @IBOutlet weak var middleTextViewLabel: UITextView!
    
    @IBOutlet weak var bottomTextViewLabel: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topTextViewLabel.text = it
        middleTextViewLabel.text = two
        bottomTextViewLabel.text = three
    }
    
    
    
}
