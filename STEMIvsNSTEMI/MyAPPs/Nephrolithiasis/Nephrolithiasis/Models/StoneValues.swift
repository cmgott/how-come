//
//  StoneValues.swift
//  Nephrolithiasis
//
//  Created by CMG on 12/11/20.
//

import UIKit

var labArray = [Lab]()


struct StoneValues {
    
    var abnormalReport = "ABNORMALs: "
    var normalReport = "NORMALs: "
    //var labResult = labResult()
    var labResult = ""
    var labName = ""
    var normal:Bool = true
    
    //func calculateLabValue (selectedPatient: String, nextLab: String, lastValue: Float) {
    
    mutating func calculateLabValue (nextLab: String, lastValue: Float) -> String {
        
        print(nextLab)
        print(lastValue)
        print("calculateLabValues initiated")
    
        
        
        if nextLab == "Urine Ca (Slider)" {
            labName = "Urine Ca"
            if lastValue > 2.5 {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Abnormally High!"
    
                normal = false
                
                
                //print("Well", labResult)
                
            } else {
                normal = true
            }
        }
        
        if nextLab == "Urine oxalate (Slider)" {
            labName = "Urine Ca"
            
            if lastValue > 4.0 {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Too Much!"
                normal = false

            } else {
                normal = true
            }
        }
        if nextLab == "Urine citrate (Slider)" {
            labName = "Urine citrate"
            if lastValue < 4.5 {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Too Little!"
                normal = false
            } else {
                normal = true
            }
        }
        if nextLab == "Urine PH" {
            if lastValue < 5.5 {
                labResult = nextLab + ": " + String(format: "%0.1f", lastValue) + " = Too Acidic"
                normal = false
            }
            if lastValue > 7.0 {
                labResult = nextLab + ": " + String(format: "%0.1f", lastValue) + " = Too Alkaline"
                normal = false
            } else {
                normal = true
            }
        }
        if nextLab == "U/uric acid (Slider)" {
            labName = "Urine UA"
            if lastValue > 0.8 {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = TOO HIGH!"
                normal = false
            } else {
                normal = true
            }
        }
        if nextLab == "Urine Vol (Slider)" {
            labName = "Urine Vol"
            if lastValue < 2 {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Dehydrated"
                normal = false
            } else {
                normal = true
            }
            
        }
        
        if nextLab == "ssCaOxalate (Tap)" {
            labName = "ssCaOxalate"
            if lastValue > 1 {
                labResult = labName + ":"  + String(format: "%0.1f", lastValue) + " = Promotes Stone Formation!"
                normal = false
            }
            if lastValue < 1 {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Helps Prevent Stone Formation!"
                normal = true
            } else {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Stablizes Stone Status"
                normal = true
            }
            
        }
        
        if nextLab == "ssCap (TAP)" {
            labName = "ssCaP"
            if lastValue > 1 {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Promotes Stone Formation!"
                normal = false
            }
            if lastValue < 1 {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Helps Prevent Stone Formation!"
                normal = true
            } else {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Stablizes Stone Status"
                normal = true
            }
            
        }
        
        if nextLab == "ssUA (TAP)" {
            labName = "ssUA"
            if lastValue > 1 {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Promotes Stone Formation!"
                normal = false
            }
            if lastValue < 1 {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Helps Prevent Stone Formation!"
                normal = true
            } else {
                labResult = labName + ": " + String(format: "%0.1f", lastValue) + " = Stablizes Stone Status"
                normal = true
            }
            
        }
        

        if normal == false {
            abnormalReport = abnormalReport + "\n" + labResult
            
           print(abnormalReport)
            
        }
        if normal == true {
            normalReport = normalReport + "\n" + labResult
            print(normalReport)
        }
        
        return abnormalReport + normalReport

    }
    
    func advice(patient: UIButton?, lab: String, labValue: UISlider?) {
        

        
    }
}

