//
//  CategoryTableViewController.swift
//  How Come?
//
//  Created by CMG on 8/13/21.
//

import UIKit
import ChameleonFramework



class HowDoesTableViewController: UITableViewController {
    
    
    var occurrences = [Occurrence]()
    var color: UIColor?
    
    func ruptureOccurrence() -> Occurrence {
        let rupture = Occurrence()
        rupture.title = "Some MIs rupture. Why?"
        rupture.medicalOccurrence = (imageName: "Rupture", text1: "It is likely that TypeI AMI is due to total epicadial coronary obstruction as demonstrated by DeWood's report on emergent CABG in AMI(A). There are numerous reports of decrease in total obstruction observed in relation to time from event onset culminating in approximately 60% patency rate at 6 months(B).\n\n" +
            "The progression of necrosis is endocardium to epicardium, usually reaching its maximum in 3-4 hours, in humans(C). ", text2: "Adequate collaterals allow for viability of epicardial myocardial cells, resulting in 'normal' evolution of ST-segment & Twaves. Inadequate collaterals result in full thickness necrosis.The latter results in localized pericardial inflammation and ECG changes and clinical findings describes as 'Regional Pericarditis', persistent ST elevation/upright Twaves(D)", text3: "ECG changes evolve over time in AMI(E). ST segment elevation and positive Twaves evolve within 48-72 hours with reduction of ST segments and inversion of Twaves. Occasionally ST segemnts remain elevated and T waves remain/become upright longer thant 48-72 hrs. This pattern is maybe accompanied by clinical markers of pericarditis(typical pain and/or rub), prompting the term 'Regional Pericarditis'. Olivia has described this and noted it's association with transmural mycardial necrosis and local pericarditis at autopsy(F).", text4: "Myocaridial rupture following AMI is associated with transmural AMI as confirmed by autopsy & surgical observation(G). Regional pericarditis is confirmatory of necrosis reaching the pericardium and documents the necrosis is 'transmural'. 'Regional Pericarditis' by ECG has been confirmed at autopsy/surgical observation as localized pericarditis(G).", text5: "Third Universal Definition of Myocardial Infarction defines myocardial infarction as ischemia induced myocardial necrosis (H) and Type I when 'related to atherosclerotic plaque rupture ... resulting in intraluminal thrombus ... leading to decreased myocardial blood flow ... with ensuing myocyte necrosis (I).", textReferences: "Some MIs rupture. Why?\n\n" + "PROPOSED HYPOTHESIS:\n" + "\tThere must be an explaination for why some Type I AMIs5️⃣ rupture and others do not.The likely hypothesis is that myocardial death must extend to the serosal pericardium ('transmural MI')4️⃣, allowing the force of ejection to rent throught the necrotic muscle, setting up to potential for rupture. If there are layers of viable myocardium abutting the serosal pericardium, the pericardal space is protected by the strength of the viable muscle and rupture is prevented ('non-transmural MI').3️⃣ Furthermore it is hypothesized that these 2 types of MI can potentially be destingiushed by the ECG, rendering a mechanism to potentially identify MIs with the potential to rupture.3️⃣ The underlying hypothesis is that the pathophysiology leading to transmural is the lack of collaterals to the ischemic myocardium.2️⃣ \n\n" + "REFERENCES: \n" + "(A)DeWood NEJM1980;303:897-902 \n" + "(B)TAMICirc1992;85:2090-2099 \n" + "(C)Rentrop AHJ 2015;170:971-980 \n" + "(D)AJC Vol.76 Oct.1, 1995:717-8 \n" + "(E) Olivia Clin.Cardiolo.17,471-478,1994 \n" + "(F)JACC Vol.22.,No.3 Sept1993:720-6 \n" + "(G)JACC Vol.22.,No.3 Sept1993:720-6 \n" + "(H)JACC Vol.60 2012, pg.3\n" + "(I)JACC Vol.60 2012 pg.7")
        return rupture
            }
    
    func clubbingOccurrence() -> Occurrence {
        

        let clubbing = Occurrence()
        clubbing.title = "Clubbing occurs with some congenital heart diseases. Why?"
        clubbing.medicalOccurrence = (imageName: "Clubbing", text1: "The postulated cause of clubbing of the fingers and toes is the release of vasoactive proteins from megakaryocytes and platelets clumps, especially platelet-derived growth factor (PDGF). These blood constituents get stuck in the capillaries of these digits releasing vasoactive proteins (PDGF) which results in the clinical findings referred to as clubbing(Dickinson EJCI(1993)23, 330-338).\n\n" + "Dickinson also describes circumstances like endocarditis were platelet clumps may embolize to the capillaries of fingers and/or toes. There they breakup releasing PDGF and other vasoactive proteins leading to clubbing.", text2: "Text2 Clubbing", text3: "Text3 Clubbing", text4: "Text4 Clubbing", text5: "Text5 Clubbing", textReferences: "Clubbing occurs with some congenital heart diseases. Why?\n\n" + "POSTULATED HYPOTHESIS: \n" + "\tThe prevaling hypothesis is that vasoactive protiens induce soft tissue changes in the nail beds of fingers and toes resuting in 'clubbing' of these digits . These protiens are released from platelets and megakarocytes as they fragment in the digit's capillaries, especially Platelet Derived Growth Factor (PDGF). Normally the megakarocytes are fragmented as they pass thorugh the lung capillaries, releasing vasoactive proteins (especially PDGF). The high flow rate of the lung capillaries 'dilute' activity of these vasoative protiens. If there is a shunt, bypassing the lungs then the megakarocytes, containing the platelets and vasoactive protiens are carried by arteries to terminal capillaries where megakerocytes become lodged and fragment. Platelets and vasoactive proteins are released. If this occurs in the terminal capillaries of the digits anatomic changes are induced, 'clubbing'(A). \n" + "\t Additionally circumstances like endocarditis were platelet clumps may embolize to the capillaries of fingers and/or toes may also result in 'clubbing'(A)\n\n" + "REFERENCES: \n" + "(A)Dickinson EJCI(1993)23, 330-338")

        return clubbing
    }
    
    func pleuralPericardialEffusionOccurrence() -> Occurrence {
        
        let pleuralPericardialEffusion = Occurrence()
        
        pleuralPericardialEffusion.title = "Pericardial effusions are not seen with left heart failure. Why?"
        
        pleuralPericardialEffusion.medicalOccurrence = (imageName: "PleuralPericardialEffusion", text1: "Elevation of left atrial(LA)/pulmonary capillary wedge(PCW) pressure is a cause of pleural effusions. Both the visceral and parietal pleural lymphatics drain the pleural space. Abnormal pleural effusion of hydrodynamic origin are directly related to LA/PCW pressure and in circumstances of normal oncotic pressure appear with elevations of > 15 mmHg.", text2: "Elevation of right atrial(RA) pressue is a cause of pericardial effusion. The pericardial space fluid hydrodynamics relate to the lymphatics of the parietal pericardium which drain into the thoracic duct which drain into central venous system near left subclavian and left internal juguluar veins(A). Abnormal pericardial fluid accumulation is directly related to RA filling pressure > 10 mmHg, in normal oncotic states(B). ", text3: "", text4: "", text5: "", textReferences: "Pericardial effusions are not seen with left heart failure. Why?\n\n" + "POSTULATED HYPOTHESIS: \n" + "\t Why is the general clinical teaching that pericardial effusions do not occur with left heart failure and pleural effusions do not occure with pulmonary hypertension? This is thought due to the lymphatic drainage of the pericardium into the right atrium1️⃣ while the pleural spaces lymphatics drain into the left atrium2️⃣.\n\n" + "REFERENCES:\n" + "(A)Vogiatzidis et al.fphys Mar2015(6) \n" + "(B)Chopra et al AmJMedSci.2021;361(6):731-735)")
        
        return pleuralPericardialEffusion
        
        
    }
    
    func stSegmentAndAMIOccurrence() -> Occurrence {
        
        let stSegmentAndAMI = Occurrence()
        
        stSegmentAndAMI.title = "ST segment elevation is critically important in the ER. Why?"
        
        stSegmentAndAMI.medicalOccurrence = (imageName: "STEMI", text1: "As defined in the Third Universal Definition of Myocardial Infaction: Type I AMI is caused by total obstruction of an epicardial coronary artery by atheromatous plaque rupture along with thrombus formation which leads to transmural myocardial ischemia. As described by Marriott, this 'pathophysologic process: transmural ischemia' is reflected by the ECG as ST elevation and if persist long enough results in AMI(A). Therefore, in the appropriate clinical setting of suspicion for acute myocardial ischemia, the finding of pathologic ST elevation acts as a highly specific marker for total epicarial coronary artery obstruction which should trigger consideration for rapid reperfusion therapy (i.e., thrombolytic therapy, CABG or PCI).", text2: "ST depression in the setting of ST elevation due to transmural myocardial ischemia is often referred to as reciprocal depression, a mirror of the ST elevation. When ST depression occurs in conjunction with myocardial ischemia, i.e., with stress testing, it is thought to represent subendocardial ischemia(B), due to mismatch of oxygen supply to demand.", text3: "", text4: "", text5: "", textReferences: "ST segment elevation is critically important in the ER. Why?\n\n" + "PROPOSED HYPOTHESIS: \n" + "\t Transmural myocardial ischemia is refelcted on the ECG by ST-Segment (ST) elevation2️⃣. Subendocardial ischemia is reflected by ST depression1️⃣. Narrow differences in O₂ supply vs demand produces subendocardial ischemia. The subendocardium has higher O₂ demand and lower blood flow compared to epicardial cells. Therefore when O₂ demand exceeds suppy subendocardial ischemia occurs and expresses itself, when able, on the ECG as ST depression. When blood supply is abruptly and 'completely' compromised, as in totally obstructed coronary artery, then transmural ischemia occurs, expressing itself, when able, on the ECG as ST elevation. In the appropriate clinical circumstance ST elevation is a highly specific marker for a totally obstructed epicardial coronary artery as the immediate factor resulting in acute myocardial ischemia, which if persist will result in necrosis, AMI. Since this cicrumstance could be aborted by specific therapy it is important to rapidly identify allowing an attempt at reperfusion therapy. \n\n" + "REFERENCES:\n" + "\t(A)NEJM 2003;349:2128-35 \n" + "\t(B)Circ Res.1998;82;957-970")
        
        return stSegmentAndAMI
        
        
    }
    
    func STEMINonSTEMIOccurrence() -> Occurrence {
        
        let STEMINonSTEMI = Occurrence()
        
        STEMINonSTEMI.title = "We classify Type I MIs as STEMI or NonSTEMI. Why?"
        
        STEMINonSTEMI.medicalOccurrence = (imageName: "STEMIvsNSTEMI", text1: "The present understanding is the a totally obstructed epicardial coronary artery leads to transmural myocardial ischemia which is reflected on the ECG as ST elevation. If the total obstruction last long enough myocardial necrosis results(A). \n" + "However, these changes may not be noted on the ECG due to several potential reasons: \n" + "\t Some myocardium is not well 'seen' by the ECG (specifically the distribution of the posterior descending and left circumflex coronary arteries(B) \n" + "\t The ECG changes may be transient and therefore missed or overlooked by readers(C) \n" + "\t The ECG changes maybe obscured by meds or the ECG itself as with LVH etc.", text2: "The ST segments and T waves evolve over the next 24-72 hours with resolution of the ST segment elevation and inversion of the T waves(D). Q waves/loss of R waves may occcur which are consistent with myocardial necrosis(E).", text3: "The central issue is that STEMI & NSTEMI are anatomically and pathophysiologically indistinquishable. Specifically total coronary artery occlusion is the anatomic finding in most all Type I MIs(E) identified with ST elevation. This is confirmed by angiography and direct visualization at time of CABG(F). The closer to the onset of the event the higher the percentage of total obstruction. Spontaneous reperfusion occurs frequently(G). This is also the case for NSTEMIs, with total obstruction found at least 25% of the time even after more than 24 hours from the onset of the event(H). This pattern results in transmural myocardial ischemia(A) and would be displayed as ST elevation on ECG(A).", text4: "The natural history and complications of both SEMI and NSEMI vary according to article reviewed but there is no clarity that the course is dictated by the ECG(I). The known risk factors, degree of LV function, inducible ischemia and anatomic vessel disease along with age, smoking etc, appear to determine the prognosis(J,K).\n" + "\tThe attempt at classification of Type I MIs based on ECG with STEMI vs NSTEMI is basically no different than the prior classification based on Q Wave vs Non-Q Wave MIs and as pointed out by Phibbs and Spodick et al(L) maybe 'Meaningless'.", text5: "It would appear that a classification system based on the pathophysiology of the specific MI event would give more understanding and identification of therapies that would have potential benefit on prognosis. The understanding of whether the infarct related artery reperfused in a timely fashion to allow viable residual myocardium in the culprit vessel distribution could lead to understanding the unique potential for recurrent ischemia/infarction or potential rupture. There are specific markers of reperfusion including visualization, biomarker patterns and identificaton of viability by imaging. There is a specific marker of transmural necrosis. This 'Regional Pericarditis' marker on ECG identifies patients with transmural necrosis which reflect no potential for recurrent ischemia in that vessel distribution but raises potential for rupture(A,M).", textReferences: "We classify Type I MIs as STEMI or NonSTEMI. Why?\n\n" + "POSTULATED HYPOTHESIS:\n" + "The ECG cannot predict the cardiovascular morbidity/mortality based on STEMI vs NonSEMI1️⃣ or by Q wave vs nonQ wave MI2️⃣. The distinction of Type I MI based on the presence or absence of ST-segement elevation or Qwaves are used as criteria to direct treatment options due to perceived difference in cardiovascular morbidity/mortality directly related to these ECG changes. Specifically early intervention with directed goal for reperfusion therapy. The underlying concept appears to be that a NonSTEMI has the potential for worse outcome due to something related to the culprit vessel and if revascularization is performed the prognosis is improved. However, the anatomy is not clearly different in STEMI vs NonSTEMI3️⃣ and the prognosis is determined by other factors not specifically identified by these ECG changes4️⃣.It would appear that therapy directed toward identifying potential recurrent ischemia would be abetter focus for our attention5️⃣. \n\n" + "REFERENCES: \n" +  "\t(A)Wang,Asinger,Marriott.ST-Segment Elevation in Conditions Other Than Acute Myocardial InfarctionN NEJM 2003;349:2128-35 \n" + "\t(B)Khan,Golwala,Tripathi,Bin Abdulhak,Bavishi,Riaz et al.Impact of total occlusion of culprit artery in acute non-STelevationmyocardial infarction:a systematic review and meta-analysis EHJ(2017)38,3082–3089 \n" + "\t(C)McCarthy,Beshansky JR,D'Agostino, et al. Missed diagnoses of acute myocardial infarction in the emergency department:Results from a multicenter study.AnnEmergMed1993;22:579-582 \n" + "\t(D)Oliva,Hammill. The Clinical Distinction between Regional Postinfarction Pericarditis and Other Causes of Postinfarction Chest Pain:Ancillary Observations Regarding the Effects of Lytic Therapy upom the Frequency of Postinfarction Pericarditis,Postinfarction Angina and Reinfarction.ClinCardiol.17,471-478(1994) \n" + "\t(E)Third Universal Definition of Myocardial Infarction.JACC Vol.60,No.x,2012 \n" + "\t(F) DeWood et al.Prevalence of total coronary occlusion during the early hours of transmural myocardial infarction. NEJM 1980;303:897-902. \n" + "\t(G)Topol et al.TAMI-6.A Randomized Trial of Late Reperfusion Therapy for Acute Myocardial Infarction.Circ.1992;85:2090-2099. \n" + "\t(H)Khan et al.Impact of total occlusion of culprit artery in acute non-STelevationmyocardial infarction:a systematic review and meta-analysis.EHJ (2017) 38, 3082–3089. \n" + "\t(I)Yang et al.Non-ST-elevated myocardial infarction with “N” wave on electrocardiogram and culprit vessel in left circumflex has a risk equivalent to ST-elevated myocardial infarction.Clin Cardiol. 2020;43:491–499. \n" + "t(J) Ahmandi et al. Prognostic Determinants of Coronary Atherosclerosis in Stable Ischemic Heart Disease Anatomy, Physiology, or Morphology? Circ Res. 2016;119:317-329. \n" + "\t(K) Cassar et al. Chronic Coronary Artery Disease: Diagnosis and Management. Mayo Clin Proc.2009;84(L):1130-1146. \n" + "\t(12)Phibbs et al.Q-Wave Versus Non-Q Wave Myocardial Infarction: A Meaningless Distinction.JACC Vol.33,No.2,1999.\n" + "\t(M) Oliva et al. Cardiac Rupture, a Clinically Predictable Complication of Acute Myocardial Infarction:Report of 70 Cases With Clinicopathologic Correlations. JACCVol.22, No.3 September1993:720-6.")
        
        return STEMINonSTEMI
        
        
    }
    
    func BBlockerEarlyAMIOccurrence() -> Occurrence {
        
        let BBlockerEarlyAMI = Occurrence()
        
        BBlockerEarlyAMI.title = "Beta-Blockers early in STEMI ... Conflicting Recommendations. Why?"
        
        BBlockerEarlyAMI.medicalOccurrence = (imageName: "Wavefront", text1: "Type I acute myocardial infarct (AMI) is caused by persistent total epicardial coronary artery obstruction due to atheromatous plaque rupture and thrombus formation(A). \n\n" + "Myocardial ischemia is proportional to O₂Demand/O₂Supply(B). \n" + "\tO₂Demand is proportional to Heart \n" + "\t\tRate,Contractility,Afterload & \n" + "\t\tPreload. \n" + "\tO₂Supply is proportional to coronary\n" + "\t\tblood flow.", text2:  "Myocardial Ischemia “Over Time” leads to Myocardial Necrosis(AMI) which progresses from endocardium to epicardium, 'Wavefront Phenomenon'(C). The rate of myocardial necrosis is proportional to O₂Demand. Usually begining when obstruction is > 30 minutes, lasting approximately 6 hours. At this point the AMI is considered 'complete'. AMI maybe limited if reperfusion occurs before the AMI is 'complete', resulting in 'myocardial savage'. This 'myocardial salvage' substracted from the initial potential size of the infarct results in the final infarct size. The final infarct size is inversely proportional amount 'myocardial salvage' and proportional to O₂Demand & duration of total obstruction(C,D).", text3: "Reperfusion prior to 'complete' infarction stops the 'Wavefront Phenomenon'. \n\n" + "O₂Demand (ie. heart rate, contractility, afterload & preload) is directly and proportional related to the speed of the 'Wavefront Phenomenon'. Betablockade decrease O₂Demand by decreasing heart rate,contractility & afterload(ie.blood pressure),potentially decreasing final infarct size(D,E).\n\n" + "If reperfusion occurs prior to 'complete' infarction the final infarct size is reduced(C). Beta Blockers, by decreasing O₂Demand, slow the 'Wavefront Phenomenon' resulting in 'myocardial salvage', if reperfusion occurs prior to 'complete' infarction. This results in a reduced final infarct size. Reperfusion prior to 'complete' infarction is mandatory for 'myocardial salvage'.\n\n" + "'Total Coronary Obstruction Time' is proportional to the final infarct size & can be 'stretched' by manuevors that decrease O₂Demand, specifically, Beta Blockade(E).", text4: "Why has the benefit of early B-Blockade not been consistenly confirmed? \n\n" + "Pre-Reperfusion Era Early B-Blockers: \n" + "\tNo Decrease Infarct Size: MILIS \n" + "\tImproved Mortality(I) \n\n" + "Reperfusion Era Early B-Blockers: \n" + "\tMixed Results:(E,G,G) \n\n" + "In studies where: \n" + "\tMetoprolol IV infused ≥2minutes.: \n" + "\t\tNo mortality benefit(G,H) \n" + "\tMetoprolol IV bolus/push: \n" + "\t\timproved mortality(E,I) \n" + "\t\tmyocardial salvage(E,G) \n\n" + "B-Blocker: Infusion vs Bolus/Push \n" + "\tIV bolus/push allows for 1st pass \n" + "\tdynamics to maximze cardiac \n" + "\teffects at lower total dose, allowing\n" + "\tabortion of protocol if indicated. \n" + "\tInfusions over several minutes\n" + "\tcircumvents 1st pass effects, liver \n" + "\tmetabolism deminishes cardiac \n" + "\texposure by ≥50%. These allow \n" + "\tfor larger total dosages before \n" + "\tadverse effects are noted leading \n" + "\tto potential increase in morbidity & \n" + "\tmortality, especially shock.\n" + "\tIt also delays the initiation of the\n" + "\teffects of B-Blockade, potentially \n" + "\tdecreasing the benefit of limiting \n" + "\tinfarct size. Infarct size is the major determinate of morbdiity/mortality in AMI(F).\n\n" + "In summary all studies that \n" + "\tclearly state metoprolol was \n" + "\tadministered IV bolus/push \n" + "\tshow improvement in mortality and \n" + "\tsmaller final infarct size(E,I).", text5: "", textReferences: "Beta-Blockers early in STEMI ... Conflicting Recommendations. Why?\n\n" + "POSTULATED HYPOTHESIS:\n\n" + "Intravenous metroprolol is usually administered in a process that maximize it's potential harm and minimize it's potential benefit4️⃣. In studies where it's given 'correctly' there is benefit without increase in mortality, specifically associated with cardiogenic shock4️⃣. The benefit of early administration of IV metoporolol is in slowing the necrotic wave front, by decreasing O₂Demand1️⃣. It is only effective in reducing infarct size when reperfusion occurs while viable myocardium exist2️⃣3️⃣.\n\n" + "REFERENCES:\n" + "\t(1)Third Universal Definition of Myocardial Infarction \n" + "\t(2)Pathophysiology of Myocardial Ischemia. Heart 2004;90:576-580 \n" + "\t(3)The 'Wavefront' of Myocardial Ischemic Cell Death II. Transmural Progression of Necrosis within Framework of Ischemic Bed Size (Myocardium at Risk ) and Collateral Flow. Laboratory Investigation Vol.40,No.6,p.633,1979. \n" + "\t(4)IV B-Blockade for Limiting Myocardial Infarction Size. Editorial CommentJACC Vol.67, No.18, 2016. \n" + "\t(5)METOCARD-CNIC Trial:Effect of Early Metoprolol on Infarct Size in ST-Segment Elevation MI Patients Undergoing Primary PCI. Circ. online Sept.3, 2013. \n" + "\t(6)Final Infarct size measured by CV magnetic resonance in patients with ST-elevation Myocardial Infarction predicts long-term clinical outcomes:an observtional study. Eur Heart J Cardiac Imaging 2013;14:387-95. \n" + "\t(7) COMMIT Trial. Lancet 2005;366:1622-32. \n" + "\t(8)Gusto-1 Trial. J AM Coll Cardiol 1998;32:634-40. \n" + "\t(9)Effect on Mortality of Metoprolol in Acute Myocardial Infarction. Lancet Oct. 17, 1981.")
        
        return BBlockerEarlyAMI
        
        
    }
    
    func aorticRegurgOccurrence() -> Occurrence {
        let aorticRegurg = Occurrence()
        aorticRegurg.title = "Asymptomatic significant aortic regurgitation (AR) is followed yearly. Why? How?"
        aorticRegurg.medicalOccurrence = (imageName: "AR", text1: "Symptomatic severe AR should be addressed with valve replacement irrespective of LV function, unless there are mitigating circumstances.\n\n" + " Asymptomatic severe AR requires consideration for surgical intervention if there is signigicant aortopathy or volume overload related LV systolic dysfunction or prominent LV diastolic enlargement. All these conditions can be evaluated by transthoracic echocardiography. The guidelines recommend yearly follow up with echo (if AR is mild to moderate echo considered every 2 years). They suggest shorter interval for situations where the criteria for intervention are borderline, as outlined in explaination #2.", text2: "All patients with asymptomatic AR require further assessment with echocardiogram to insure that several other markers hearlding potential deterioration are not present. There presence should lead to consideration for procedural therapy because studies have documented better outcomes with surgical intervention. These findings include: \n" + "Aortopathy: Root \n" + "\t ≥55mm \n" + "\t ≥50mm for Bicuspid AV/Marfan's \n" + "\t ≥45mm genetic Marfan's/with AVR \n" + "\t ≥40mm ♁ low BSA \n" + "LVEF ≤50% (50-55%*) \n" + "LVESD ≥50mm \n" + "LVESDI ≥ 25mm/mm2 (20-22*) \n" + "LVED >65mm \n" + "* Surgical RX if low risk", text3: "If one of these triggers are present guidelines suggest surgical therapy. If risk precude this approach then 'ACEI or dihydropyridine calcium channel blockers may be used for symptomatic relief'. If there are questions of symptoms stress testing should be utilized.", text4: "4", text5: "5", textReferences: "2021 ESC/EACTS Guidelines for the management of valvular heart disease. European Heart Journal(2021)00, 1-72")
        
        return aorticRegurg
    }
    
    func secondDegreeAVBlockOccurrence() -> Occurrence {
        let secondDegreeAVBlock = Occurrence()
        secondDegreeAVBlock.title = "2ndDegree AV Block only sometimes requires pacing; Mobitz Classification is 'outdated'. Why?"
        secondDegreeAVBlock.medicalOccurrence = (imageName: "AR", text1: "Symptomatic severe AR should be addressed with valve replacement irrespective of LV function, unless there are mitigating circumstances.\n\n" + " Asymptomatic severe AR requires consideration for surgical intervention if there is signigicant aortopathy or volume overload related LV systolic dysfunction or prominent LV diastolic enlargement. All these conditions can be evaluated by transthoracic echocardiography. The guidelines recommend yearly follow up with echo (if AR is mild to moderate echo considered every 2 years). They suggest shorter interval for situations where the criteria for intervention are borderline, as outlined in explaination #2.", text2: "All patients with asymptomatic AR require further assessment with echocardiogram to insure that several other markers hearlding potential deterioration are not present. There presence should lead to consideration for procedural therapy because studies have documented better outcomes with surgical intervention. These findings include: \n" + "Aortopathy: Root \n" + "\t ≥55mm \n" + "\t ≥50mm for Bicuspid AV/Marfan's \n" + "\t ≥45mm genetic Marfan's/with AVR \n" + "\t ≥40mm ♁ low BSA \n" + "LVEF ≤50% (50-55%*) \n" + "LVESD ≥50mm \n" + "LVESDI ≥ 25mm/mm2 (20-22*) \n" + "LVED >65mm \n" + "* Surgical RX if low risk", text3: "If one of these triggers are present guidelines suggest surgical therapy. If risk precude this approach then 'ACEI or dihydropyridine calcium channel blockers may be used for symptomatic relief'. If there are questions of symptoms stress testing should be utilized.", text4: "4", text5: "5", textReferences: "2021 ESC/EACTS Guidelines for the management of valvular heart disease. European Heart Journal(2021)00, 1-72")
        
        return secondDegreeAVBlock
    }
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        occurrences = [ruptureOccurrence(), clubbingOccurrence(), pleuralPericardialEffusionOccurrence(), stSegmentAndAMIOccurrence(), STEMINonSTEMIOccurrence(),BBlockerEarlyAMIOccurrence() ,aorticRegurgOccurrence(),secondDegreeAVBlockOccurrence()]

        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return occurrences.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "howDoesCell", for: indexPath)
        
        //Making random colors with appropriate text color
        color = UIColor.randomFlat()
        
//        if let color = color?.darken(byPercentage: (CGFloat(indexPath.row)/CGFloat(occurrences.count))/2) {
        cell.backgroundColor = color
        let textColor = UIColor(contrastingBlackOrWhiteColorOn: color!, isFlat: true)
        
        
            cell.backgroundColor = color
            cell.textLabel?.textColor = textColor
        
        //}
        cell.textLabel?.text = occurrences[indexPath.row].title
        cell.textLabel?.numberOfLines = 6
        //cell.titleLabel?.textColor =
        //}
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteIt = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            completionHandler(true)
            
        }
        
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [deleteIt])
        
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "goToWhyThing", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationVC = segue.destination as! HowDoesViewController
        if let indexPath = tableView.indexPathForSelectedRow {
            destinationVC.selectedOccurrence =
                occurrences[indexPath.row]
            
        }
    }
}



