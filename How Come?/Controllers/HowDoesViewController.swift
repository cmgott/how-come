//
//  HowDoesViewController.swift
//  How Come?
//
//  Created by CMG on 9/29/21.
//

import UIKit

class HowDoesViewController: UIViewController {
    
    var occurrences = [Occurrence]()
    var occurrence = Occurrence()
    
    var textWanted = ""
    
    var image = ""
    
    @IBOutlet weak var imageMain: UIImageView!
    
    @IBOutlet weak var text5ButtonLabel: UIButton!
    @IBOutlet weak var text4ButtonLabel: UIButton!
    @IBOutlet weak var text3ButtonLabel: UIButton!
    @IBOutlet weak var text2ButtonLabel: UIButton!
    @IBOutlet weak var text1ButtonLabel: UIButton!
    var occurence = 0
    
    var selectedOccurrence:Occurrence? {
        didSet {
            image = (selectedOccurrence?.medicalOccurrence.imageName)!
        }
    
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if selectedOccurrence!.title == "Clubbing occurs with some congenital heart diseases. Why?" {
            text5ButtonLabel.isHidden = true
            text4ButtonLabel.isHidden = true
            text3ButtonLabel.isHidden = true
            text2ButtonLabel.isHidden = true
            text1ButtonLabel.setTitleColor(.clear, for: .normal)
            
            
        }
        
        if selectedOccurrence!.title == "Pericardial effusions are not seen with left heart failure. Why?" {
            text5ButtonLabel.isHidden = true
            text4ButtonLabel.isHidden = true
            text3ButtonLabel.isHidden = true
            text2ButtonLabel.setTitle("1", for: .normal)
            text1ButtonLabel.setTitle("2", for: .normal)
            
        }
        
        if selectedOccurrence!.title == "ST segment elevation is critically important in the ER. Why?" {
            text5ButtonLabel.isHidden = true
            text4ButtonLabel.isHidden = true
            text3ButtonLabel.isHidden = true
            text2ButtonLabel.setTitle("1", for: .normal)
            text1ButtonLabel.setTitle("2", for: .normal)
            
        }
        
        if selectedOccurrence!.title == "Asymptomatic significant aortic regurgitation (AR) is followed yearly. Why? How?" {
            text5ButtonLabel.isHidden = true
            text4ButtonLabel.isHidden = true
            text3ButtonLabel.setTitle("3", for: .normal)
            text2ButtonLabel.setTitle("2", for: .normal)
            text1ButtonLabel.setTitle("1", for: .normal)
            
        }
        
        if selectedOccurrence!.title == "Beta-Blockers early in STEMI ... Conflicting Recommendations. Why?" {
            text5ButtonLabel.isHidden = true
            text4ButtonLabel.setTitle("4", for: .normal)
            text3ButtonLabel.setTitle("3", for: .normal)
            text2ButtonLabel.setTitle("2", for: .normal)
            text1ButtonLabel.setTitle("1", for: .normal)
            
        }
        
        
        imageMain.image = UIImage(named: image)
        

    }
    @IBAction func text5ButtonPressed(_ sender: UIButton) {
        
        textWanted = (selectedOccurrence?.medicalOccurrence.5)!
        performSegue(withIdentifier: "goToExplain", sender: self)
        
        
    }
    
    @IBAction func text4ButtonPressed(_ sender: UIButton) {
        textWanted = (selectedOccurrence?.medicalOccurrence.4)!
        
        performSegue(withIdentifier: "goToExplain", sender: self)
    }
    
    
    @IBAction func text3ButtonPressed(_ sender: UIButton) {
        textWanted = (selectedOccurrence?.medicalOccurrence.3)!
        performSegue(withIdentifier: "goToExplain", sender: self)
    }
    
    
    @IBAction func text2ButtonPressed(_ sender: UIButton) {
        textWanted = (selectedOccurrence?.medicalOccurrence.2)!
        performSegue(withIdentifier: "goToExplain", sender: self)
    }
    
    
    @IBAction func text1ButtonPressed(_ sender: UIButton) {
        textWanted = (selectedOccurrence?.medicalOccurrence.1)!
        performSegue(withIdentifier: "goToExplain", sender: self)
    }
    
    @IBAction func referenceButtonPressed(_ sender: UIButton) {
        textWanted = (selectedOccurrence?.medicalOccurrence.6)!
        performSegue(withIdentifier: "goToExplain", sender: self)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToExplain" {
            
            let destinationVC = segue.destination as! ExplainationViewController
            destinationVC.textWanted = textWanted

            
        }
    }
    

    
}
