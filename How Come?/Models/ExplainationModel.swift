//
//  ExplainationModel.swift
//  How Come?
//
//  Created by CMG on 9/30/21.
//

import UIKit

//tried to use to creat an array but abandoned since thought would have to persist in coreData and could not figure out how to use with hard data.

struct ExplainationModel {
    
    var occurrenceName = ""
    var occurrenceText1 = ""
    var occurrenceText2 = ""
    var occurrenceText3 = ""
    var occurrenceText4 = ""
    var occurrenceText5 = ""
    var occurrenceReferences = ""
    
}
